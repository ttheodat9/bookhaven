bookAdd = () => {
    if ($('input[type=file]')[0].files[0] == null) {
        swal('Missing Book Image!', 'Please select an image', 'error');
        return;
    }
    else if (document.getElementById('title').value === '' || document.getElementById('author').value === '') {
        swal('', 'Please fill in the title and author inputs.', 'error');
    }
    else {
        //Checking the value of the borrowed checkbox
        let response = '';
        if ($('#borrowed').is(":checked")) {
            response = 'yes';
        } else {
            response = 'no';
        }
        var book = new FormData(); //It creates a new FormData object to save the fields in the form
        book.append('bookTitle', $('#title').val());
        book.append('bookAuthor', $('#author').val());
        book.append('bookGenre', $('#genre').val());
        book.append('image', $('input[type=file]')[0].files[0])
        book.append('borrowed', response);


        $.ajax({
            url: '/addBook/save',
            method: 'POST',
            data: book,
            enctype: 'multipart/form-data', //To combine regular inputs with file-type inputs
            processData: false,  // Important!
            contentType: false,
            success: (data, status) => {
                if (status === 'success') {
                    $('.save').toast('show');
                    $('#title').val('');
                    $('#author').val('');
                    $('#image').val('');
                    $('#genre option:first').prop('selected', true);
                    $('#borrowed').prop("checked", false);
                    $("#title").focus();
                }
            }
        })
    }
}
editBook = (book) => {
    $('#edittitle').val(book.bookTitle);
    $('#editauthor').val(book.bookAuthor);
    $('#editgenre').val(book.bookGenre);
    //  $('#editimage')[0].files[0].book.image;
    // $('input[type=file]')[0].files[0] = book.image;
    $('#theID').val(book.bookID);
    if (book.borrowed === 'yes') {
        $('#editborrowed').prop("checked", true);
    } else {
        $('#editborrowed').prop("checked", false);
    }
    $('#bookModal').modal('show');


    //Onclick of the save button in the modal
    $('#saveChanges').on('click', () => {
        //Checking the value of the borrowed checkbox
        let newresponse = '';
        if ($('#editborrowed').is(":checked")) {
            newresponse = 'yes';
        } else {
            newresponse = 'no';
        }

        if (document.getElementById('edittitle').value === '' || document.getElementById('editauthor').value === '') {
            swal('', 'Please fill in the title and author inputs.', 'error');
        }
        else {

            let newID = $('#theID').val();

            var editbook = {
                bookTitle: $('#edittitle').val(),
                bookAuthor: $('#editauthor').val(),
                bookGenre: $('#editgenre').val(),
                borrowed: newresponse
            }
            $.ajax({
                url: `/listBooks/edit/${newID}`,
                method: 'PUT',
                data: editbook,
                success: (data, status) => {
                    if (status === 'success') {
                        location.reload();
                    }
                }
            })
        }
    })
}
removeBook = (bookID) => {
    swal({
        title: "Are you sure?",
        text: "Once deleted, this record cannot be recovered!",
        icon: "warning",
        buttons: true,
        dangerMode: true, 
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: `/listBooks/remove/${bookID}`,
                    method: 'DELETE',
                    success: (data, status) => {
                        if (status === 'success') {
                            location.reload();
                        }
                    }
                })
            } else {
                $('.cancel').toast('show');
            }
        });
}



readerAdd = () => {
    if (document.getElementById('fname').value === '' ||
        document.getElementById('lname').value === '' ||
        document.getElementById('address').value === '' ||
        document.getElementById('phone').value === '') {
        swal('', 'Please fill in all inputs.', 'error');
    }
    else {
        //Checking phone input
        var phone = $('#phone').val();
        let phoneVerification = 0;
        for (const digit in phone) {
            if (!isNaN(digit)) {
                phoneVerification++;
            }
        }
        if (phoneVerification < 10 || phoneVerification > 10) {
            swal('', 'Please fill in the phone input correctly.', 'error');
        }
        else {
            var reader = {
                readerFirstname: $('#fname').val(),
                readerLastname: $('#lname').val(),
                readerAddress: $('#address').val(),
                readerPhone: phone
            }
            $.ajax({
                url: '/addReader/save',
                method: 'POST',
                data: reader,
                success: (data, status) => {
                    if (status === 'success') {
                        $('.save').toast('show');
                        $('#fname').val('');
                        $('#lname').val('');
                        $('#address').val('');
                        $('#phone').val('');
                        $("#fname").focus();
                    }
                }
            })
        }
    }
}
editReader = (reader) => {
    $('#editfname').val(reader.readerFirstname);
    $('#editlname').val(reader.readerLastname);
    $('#editaddress').val(reader.readerAddress);
    $('#editphone').val(reader.readerPhone);
    $('#readID').val(reader.readerID);
    $('#readerModal').modal('show');

    //Onclick of the save button in the modal
    $('#readerChanges').on('click', () => {
        if (document.getElementById('editfname').value === '' ||
            document.getElementById('editlname').value === '' ||
            document.getElementById('editaddress').value === '' ||
            document.getElementById('editphone').value === '') {
            swal('', 'Please fill in all inputs.', 'error');
        }
        else {
            let newID2 = $('#readID').val();
            var editReader = {
                readerFirstname: $('#editfname').val(),
                readerLastname: $('#editlname').val(),
                readerAddress: $('#editaddress').val(),
                readerPhone: $('#editphone').val(),
            }
            $.ajax({
                url: `/listReaders/edit/${newID2}`,
                method: 'PUT',
                data: editReader,
                success: (data, status) => {
                    if (status === 'success') {
                        location.reload();
                    }
                }
            })
        }
    })
}
removeReader = (readerID) => {
    swal({
        title: "Are you sure?",
        text: "Once deleted, this record cannot be recovered!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: `/listReaders/remove/${readerID}`,
                    method: 'DELETE',
                    success: (data, status) => {
                        if (status === 'success') {
                            location.reload();
                        }
                    }
                })
            } else {
                $('.cancel').toast('show');
            }
        });
}


//RENTS
//Initializing the search component on the select2 --- IMPORTANT
$(document).ready(() => {
    $('#reader').select2({});
    $('#reader2').select2({});
    $('#book').select2({});
    //Setting values of the date something is rented and returned
    var now = new Date();
    var month = (now.getMonth() + 1);
    var oneMonth = ((now.getMonth() + 1) + 1);

    var day = now.getDate();
    if (month < 10)
        month = "0" + month;
    if (oneMonth < 10)
        oneMonth = "0" + oneMonth;
    if (day < 10)
        day = "0" + day;
    var today = now.getFullYear() + '-' + month + '-' + day;
    var inAmonth = now.getFullYear() + '-' + oneMonth + '-' + day;
    $('.rentDate').val(today);
    $('.returnDate').val(inAmonth);
});

rentBook = (book) => {
    //Displaying the modal
    $('.modal-title').html('Rent \"' + book.bookTitle + '\"?');
    $('#rentBookID').val(book.bookID);
    $('#rentModal').modal('show');

    $('#rentNow').on('click', () => {
        let rent = {
            loanDate: $('.rentDate').val(),
            returnDate: $('.returnDate').val(),
            readerID: $('#reader').val(),
            bookID: $('#rentBookID').val()
        };

        $.ajax({
            url: '/addRent/save',
            method: 'POST',
            data: rent,
            success: (data, status) => {
                if (status === 'success') {
                    $('#rentModal').modal('hide');
                    $('.toast-body').html('Your rent was saved.');
                    $('.save').toast('show');

                }
            }
        })
    })
}
editRent = (loan) => {
    $('#reader2').val(loan.readerID).trigger('change');
    $('#book').val(loan.bookID).trigger('change');
    $('#rentModal').modal('show');

    let updatedReader = '';
    let updatedBook = '';
    if ($('#reader2').val('')) {
        updatedReader = loan.readerID;
    } else {
        updatedReader = $('#reader2').val();
    }

    if ($('#book').val('')) {
        updatedBook = loan.bookID;
    } else {
        updatedBook = $('#book').val();
    }

    let theID = $('#theID3').val();
    let updatedRent = {
        loanDate: $('.rentDate').val(),
        returnDate: $('.returnDate').val(),
        readerID: updatedReader,
        bookID: updatedBook
    }

    $('#rentChanges').on('click', () => {
        $.ajax({
            url: `/listRents/edit/${theID}`,
            type: 'PUT',
            data: updatedRent,
            status: (data, status) => {
                if (status === 'success') {
                    location.reload();
                }
            }
        })
    })
}
removeRent = (rentID) => {
    swal({
        title: "Are you sure?",
        text: "Once deleted, this record cannot be recovered!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: `/listRents/remove/${rentID}`,
                    method: 'DELETE',
                    success: (data, status) => {
                        if (status === 'success') {
                            location.reload();
                        }
                    }
                })
            } else {
                $('.cancel').toast('show');
            }
        });
}