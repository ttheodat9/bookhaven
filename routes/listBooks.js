var express = require('express');
const connection = require('../mysqlConnect/connect');
var router = express.Router();
var formidable = require('formidable');
var multiparty = require('multiparty');


/* GET list of books. */ 
router.get('/', (req, res) => { 
    res.render('listBooks');
}); 
router.get('/get', (req, res) => {
  let showCmd = 'SELECT * FROM books';
  connection.query(showCmd, (error, result) => {
    if (error) throw error;
    res.end(JSON.stringify(result));
  })
}); 

router.post('/save',  (req, res) => {
   //This section saves the image in the folder public -> images using the module formidable
   const input = new formidable.IncomingForm();
   input.uploadDir = 'images';
   input.parse(req);
   input.on('fileBegin', (field, file) => {
     file.path = "./public/images/" + file.name;
   });
   //This section parses the form using the module multiparty
   var formData = new multiparty.Form();
   formData.parse(req, (error, fields, files) => { //The callback function returns error if any, fields(properties), and files loaded
     var rowData = [
       fields.bookTitle,    
       fields.bookAuthor,     
       fields.bookGenre,
       files.image[0].originalFilename,
       fields.borrowed
     ];
     let saveCmd = 'INSERT INTO books(bookTitle, bookAuthor, bookGenre, image, borrowed) VALUES (?)'
     connection.query(saveCmd, [rowData], (error, result) => {
       if (error) throw error;
       res.end(JSON.stringify(result.insertId)); //sends the ID of the last record inserted
     });
   });
 })
    
router.put('/edit/:bookID', (req, res) => { 
  let editIt = req.body;
  let id = req.params.bookID;  
  let editCmd = 'UPDATE books SET ? WHERE bookID = ?';
  connection.query(editCmd, [editIt, id], (error, result) => {
    if (error) throw error;
    res.end(); 
  })
})   
 
router.delete('/remove/:bookID', (req, res) => {
  let id = req.params.bookID;
  let remCmd = 'DELETE FROM books WHERE bookID = ?';
  connection.query(remCmd, id, (error, result) => {
    if (error) throw error;
    res.end();
  })  
})

module.exports = router;