var express = require('express');
const connection = require('../mysqlConnect/connect');
var router = express.Router();

/* GET list of readers page. */
router.get('/', function (req, res,) {
    res.render('listReaders' );
});
router.get('/get', function (req, res,) {
  let showCmd = 'SELECT * FROM readers';
  connection.query(showCmd, (error, result) => {
    if (error) throw error;
    res.end(JSON.stringify(result));
  });
});

router.post('/save',  (req, res) => {
  let form = req.body;
    let saveCmd = 'INSERT INTO readers SET ?';
    connection.query(saveCmd, form, (error, result) => {
      if (error) throw error;
      res.end(JSON.stringify(result.insertId)); //sends the ID of the last record inserted
    });
  });

router.put('/edit/:readerID', (req, res) => {
  let edittedRead = req.body;
  let IDread = req.params.readerID;
  let editCmd = 'UPDATE readers SET ? WHERE readerID = ?';
  connection.query(editCmd, [edittedRead, IDread], (error, result) => {
    if (error) throw error;
    res.end();
  });
})

router.delete('/remove/:readerID', (req, res) => {
  let ID = req.params.readerID;
  let removeCmd = 'DELETE FROM readers WHERE readerID = ?';
  connection.query(removeCmd, ID, (error, result) => {
    if (error) throw error;
    res.end();
  });
})




module.exports = router;