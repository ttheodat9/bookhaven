var express = require('express');
const connection = require('../mysqlConnect/connect');
var router = express.Router();

/* GET list of rents page. */
router.get('/', (req, res) => { 
  res.render('listRents');
});

router.get('/get', (req, res) => {
  let lists = [
    'SELECT loans.*, readers.readerFirstname, readers.readerLastname, books.bookTitle  FROM loans INNER JOIN readers ON loans.readerID = readers.readerID INNER JOIN books ON books.bookID = loans.bookID',
    'SELECT * FROM readers',
    'SELECT * FROM books',
  ]
  connection.query(lists.join(';'), (error, result) => {
    if (error) throw error;
    res.send({
      rents: JSON.stringify(result[[0]]),
      readers: JSON.stringify(result[[1]]),
      books: JSON.stringify(result[[2]]),
    });
    res.end();
  });
});



router.post('/save', (req, res) => {
  let info = req.body;
  let saveCmd = 'INSERT INTO loans SET ?';
  connection.query(saveCmd, info, (error, result) => {
    if (error) throw error;
    res.end();
  });
})

router.put('/edit/:rentID', (req, res) => {
  let id = req.params.rentID;
  let renew = req.body;
  let editCmd = "UPDATE loans SET ? WHERE rentID =  ?";
  connection.query(editCmd, [renew, id], (error, result) => {
    if (error) throw error;
    res.end();
  });
})

router.delete('/remove/:rentID', (req, res) => {
  let id = req.params.rentID;
  let removeCmd = 'DELETE FROM loans WHERE rentID =  ?';
  connection.query(removeCmd, id, (error, result) => {
    if (error) throw error;
    res.end();
  });
})

module.exports = router;